﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = GetPeople(20);
            Print(list);
            Console.WriteLine();

            var contacts = GetContacts(list);
            Print(contacts);
            Console.WriteLine();

            //var person = contacts.Search(957554010);
            var person = contacts.Search("A13");
            Console.WriteLine(person.FullName);
            Console.WriteLine();

            Console.ReadKey();
        }
        static List<Person> GetPeople(int count)
        {
            var rnd = new Random();
            var list = new List<Person>(count);
            for (int i = 1; i <= count; i++)
            {
                var person = new Person
                {
                    Name = "A" + i,
                    Surname = "A" + i + "yan",
                    Age = i + 10,
                    Number = rnd.Next(100000000,999999999)
                };
                list.Add(person);
            }
            return list;
        }
        static void Print(List<Person> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine(item.FullName);
            }
        }
        static List<Contact> GetContacts(List<Person> people)
        {
            var contacts = new List<Contact>(people.Count);
            for (int i = 0; i < people.Count; i++)
            {
                contacts.Add(new Contact { Person = people[i]});
            }
            return contacts;
        }
        static void Print(List<Contact> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine(item.Person.FullName);
            }
        }
    }
}
