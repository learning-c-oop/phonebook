﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    static class PhoneBook
    {
        //public static List<Contact> ContactList { get; set; }
        public static Person Search(this List<Contact> contactList, string name)
        {
            var person = new Person();
            for (int i = 0; i < contactList.Count; i++)
            {
                if (contactList[i].Person.Name == name)
                {
                    person = contactList[i].Person;
                }
            }
            return person;
        }
        public static Person Search(this List<Contact> contactList, int number)
        {
            var person = new Person();
            for (int i = 0; i < contactList.Count; i++)
            {
                if (contactList[i].Person.Number == number)
                {
                    person = contactList[i].Person;
                }
            }
            return person;
        }
    }
}
